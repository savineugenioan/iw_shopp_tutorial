import { ShoppingCartService } from './../shopping-cart.service';
import { AppUser } from './../models/app-user';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {
  appUser: AppUser;
  shoppingCartItemCount : number;
  collapsed = true;
  toogleCollapsed(): void {
    this.collapsed = !this.collapsed;
  }
  constructor(private auth: AuthService,private shoppingCartService: ShoppingCartService) { 


  }

  logout() {
    this.auth.logout();
  }
    ngOnInit(){
    this.auth.appUser$.subscribe(appUser => this.appUser = appUser);
    let cart$ = this.shoppingCartService.getCart();
    cart$.snapshotChanges().subscribe(cart =>{
      this.shoppingCartItemCount = 0;
      if(cart.payload.exists())
      for(let productId in cart.payload.exportVal().items)
        this.shoppingCartItemCount += cart.payload.exportVal().items[productId].quantity;
    })
  }

}
