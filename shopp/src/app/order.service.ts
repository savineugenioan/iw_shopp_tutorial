import { ShoppingCartService } from './shopping-cart.service';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Injectable, Query } from '@angular/core';

@Injectable()
export class OrderService {

  constructor(private db: AngularFireDatabase,
    private shoppingCartService:ShoppingCartService) { }
  storeOrder(order){
    let result = this.db.list('orders').push(order);
    this.shoppingCartService.clearCart();
     return result;
  }
  getOrders() { 
    return this.db.list('/orders');
  }
  getOrdersByUser(userId: string) {
    //console.log(userId);
       let x=this.db.list('/orders/', ref =>
       ref.orderByChild('userId').equalTo(userId));
       return x;
  }
}
