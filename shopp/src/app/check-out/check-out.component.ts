import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Product } from './../models/product';
import { OrderService } from './../order.service';
import { Subscription } from 'rxjs';
import { ShoppingCartService } from './../shopping-cart.service';
import { Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnDestroy {
  shipping = {}; 
  cart ;
  userId:string;
  totalPrice :number=0;
  totalQuantity=0;
  items:Product[]=[];
  sub : Subscription;
  sub1 : Subscription;
  order;
  constructor(private shoppingCartService:ShoppingCartService,
    private authService:AuthService,
    private orderService:OrderService,
    private router:Router) {
      let cart$ = this.shoppingCartService.getCart(); 
      this.sub = cart$.snapshotChanges().subscribe(cart =>{
        let x=cart.payload.exportVal().items;
        for(var i in x){ 
          x[i].totalprice = x[i].quantity*x[i].product.price;
          this.totalPrice += x[i].quantity*x[i].product.price;
          this.totalQuantity+=x[i].quantity;
          this.items.push(x[i]);
        }   });
        this.sub1 = this.authService.user$.subscribe(user =>{
        this.userId = user.uid;
      });
      //console.log(this.items);
      
    }
  
  placeOrder() {
      this.order = {
      userId : this.userId,
      datePlaced: new Date().getTime(),
      shipping: this.shipping,
      items: this.items,
      quantity : this.totalQuantity,
      totalPrice : this.totalPrice,
    };

    //console.log(order);
    let result = this.orderService.storeOrder(this.order);

    //console.log(result);
    
    this.router.navigate(['/order-success',result.key])
  } 
  ngOnDestroy()
  {
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
  }
}
