import { Product } from './models/product';
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable} from '@angular/core';
import 'rxjs/operator/take';
import { Subscription } from 'rxjs';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService{
  sub:Subscription;
  res;
  totalPrice;
  constructor(private db : AngularFireDatabase) { 

  }
  getere(){
    let x:ShoppingCartComponent
    this.res= x.res;
    this.totalPrice=x.Totalprice;
  }
  private create() {
    return this.db.list('/shopping-carts/').push({
      dateCreated: new Date().getTime(),
    })
  }
  getCart(){
    let cartId = this.getOrCreateCartId();
    
    return this.db.object('/shopping-carts/' + cartId);
  }
  private getItem(cartId,productId){
      return this.db.object('/shopping-carts/'+ cartId + '/items/'+ productId);
  }
    private  getOrCreateCartId(){
      let cartId = localStorage.getItem('cartId');
      if(cartId) return cartId;

      let result = this.create() ;
      localStorage.setItem("cartId",result.key);
      return result.key;    
    }
     addToCart(product: Product){
      let cartId= this.getOrCreateCartId();
      let item$ = this.getItem(cartId,product.key);
      
        item$.snapshotChanges().take(1).subscribe(item =>{
          if(item.payload.exists()) item$.update({ product:product, quantity: (item.payload.exportVal().quantity)+1});
        else {item$.set({ product:product, quantity: 1});}
      })
    }
     removeFromCart(product: Product){
      let cartId= this.getOrCreateCartId();
      let item$ = this.getItem(cartId,product.key);
        item$.snapshotChanges().take(1).subscribe(item =>{
          if(item.payload.exportVal().quantity>1) item$.update({ product:product, quantity: (item.payload.exportVal().quantity)-1});
        else item$.remove();
      })
    }
        clearCart(){
        let cartId=this.getOrCreateCartId();
        this.db.object('/shopping-carts/'+cartId+"/items").remove()
      }
        
      }
