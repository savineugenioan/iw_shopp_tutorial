import { Product } from './../../models/product';
import { ProductService } from './../../product.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent{
  products$: Product[]=[];
  filtredproducts;
  constructor(private productsService:ProductService,private db:AngularFireDatabase) {
    let obj;
    obj = productsService.getAll().snapshotChanges().forEach(data =>{
      data.map(dt=>{
        this.products$.push({
          key: dt.payload.key,
          title : dt.payload.exportVal().title,
          price : dt.payload.exportVal().price,
          category : dt.payload.exportVal().category,
          imageUrl : dt.payload.exportVal().imageUrl
        });
        this.filtredproducts=this.products$;
      });
    });
  }
  filter(query: string){
    this.filtredproducts = (query) ?
    this.products$.filter(p => p.title.toLowerCase().includes(query.toLowerCase())) : this.products$;
    
  }
}
