//import { Order } from './../../models/order';
import { OrderService } from './../../order.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-orders',
  templateUrl: './admin-orders.component.html',
  styleUrls: ['./admin-orders.component.css']
})
export class AdminOrdersComponent {
  orders$;
  orders =[];
  constructor(private orderService: OrderService) { 
    this.orders$ = orderService.getOrders().snapshotChanges().forEach(order =>{
      order.map(dt=>{
        this.orders.push({
          key: dt.payload.key,
          items : dt.payload.exportVal().items,
          quantity : dt.payload.exportVal().quantity,
          shipping : dt.payload.exportVal().shipping,
          totalPrice : dt.payload.exportVal().totalPrice,
          datePlaced : dt.payload.exportVal().datePlaced,
        });
      });
    });
  }
}
