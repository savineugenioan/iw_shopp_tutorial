import { Product } from './../../../models/product';
import { ProductService } from './../../../product.service';
import { CategoryService } from './../../../category.service';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class ProductFormComponent{
public categories$ : Object[]=[];
product ={title:"",key:"",imageUrl:"",price:"",category:""};
id;

  constructor(private router : Router,private categoryService: CategoryService,private productService: ProductService,
    private route: ActivatedRoute) { 
    let obj; 

    obj = categoryService.getAll().subscribe(data =>{
      data.map(dt=>{
        this.categories$.push({
          key:dt.payload.key,
          name : dt.payload.exportVal().name
        });
      });
    });
    this.id = this.route.snapshot.paramMap.get("id");
    let obj1;
    if(this.id){
       obj1 = this.productService.getProduct(this.id).valueChanges().take(1).subscribe(data =>{
          this.product = {
            key : data.key,
            title : data.title,
            price : data.price,
            category : data.category,
            imageUrl : data.imageUrl
          };
        });
    }
  }
  save(product){
    if(this.id)
    this.productService.update(this.id,product);
    else
      this.productService.create(product);

    this.router.navigate(['/admin/products']);
  }
  delete(){
    if(!confirm("Are you sure you want to delete this product ?")) return ;
      this.productService.delete(this.id);
      this.router.navigate(['/admin/products']);
  }
}
