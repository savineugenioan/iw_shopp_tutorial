import { Component, OnInit, Input } from '@angular/core';
import { CategoryService } from 'src/app/category.service';

@Component({
  selector: 'product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.css']
})
export class ProductFilterComponent implements OnInit {
  categories$ : Object[]=[];
  @Input('category') category;
  constructor(categoryService: CategoryService) { 
    let obj;
    obj = categoryService.getAll().subscribe(data =>{
      data.map(dt=>{
        this.categories$.push({
          key:dt.payload.key,
          name : dt.payload.exportVal().name
        });
      });
    });
  }

  ngOnInit() {
  }

}
