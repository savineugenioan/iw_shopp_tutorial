import { ShoppingCartService } from './../shopping-cart.service';
import { Product } from './../models/product';
import { ProductService } from './../product.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {  ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit,OnDestroy{
  products$: Product[]=[];
  category : string;
  cart: any;
  subscription : Subscription;
  filtredproducts :Product[]=[];
  
  constructor(route: ActivatedRoute,
    productService : ProductService,
    private shoppingCartService : ShoppingCartService) { 



    let obj;
    obj = productService.getAll().snapshotChanges().forEach(data =>{
      data.map(dt=>{
        this.products$.push({
          key: dt.payload.key,
          title : dt.payload.exportVal().title,
          price : dt.payload.exportVal().price,
          category : dt.payload.exportVal().category,
          imageUrl : dt.payload.exportVal().imageUrl
        });

        route.queryParamMap.subscribe(params =>{
          this.category = params.get('category');
          this.filtredproducts = (this.category) ? this.products$.filter(p => p.category == this.category) : this.products$;
        });

      });
    });

  }

  async ngOnInit(){
     this.subscription = (await this.shoppingCartService.getCart()).snapshotChanges().subscribe(
       cart =>this.cart = cart.payload.exportVal());

  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
