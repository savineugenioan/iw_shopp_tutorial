import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db:AngularFireDatabase) { }
  create(product){
    return this.db.list('/products/').push(product);
  }
  update(productId, product){
      return this.db.object('/products/' + productId).update(product);
  }
  delete(productId){
    return this.db.object('/products/' + productId).remove();
  }

    getAll(){
      return this.db.list('/products/');
    }
    getProduct(productid): AngularFireObject<any>{
      return this.db.object('/products/'+ productid);
    }
}
