export interface Product{
    key;
    title: string;
    price: number;
    category: string;
    imageUrl: string;
}