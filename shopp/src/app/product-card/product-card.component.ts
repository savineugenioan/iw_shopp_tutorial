import { ShoppingCartService } from './../shopping-cart.service';
import { Product } from './../models/product';
import { Component, Input } from '@angular/core';
//import undefined = require('firebase/empty-import');

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent {
@Input('product') product : Product;
@Input('show-actions') showActions = true;
@Input('shopping-cart') shoppingCart;
  constructor(private cartService : ShoppingCartService) {  }
  addToCart(){
    this.cartService.addToCart(this.product);
  } 
  removeFromCart(){
    this.cartService.removeFromCart(this.product);
  }
  getQuantity(){
    if(!this.shoppingCart) return 0;
    if(this.shoppingCart.items == undefined) return 0;
    if(!this.product.key) return 0;
    console.log(this.shoppingCart.items);
    console.log(this.product.key);
    
    
    let item = this.shoppingCart.items[this.product.key];
    return item ? item.quantity :0;
  }

}
