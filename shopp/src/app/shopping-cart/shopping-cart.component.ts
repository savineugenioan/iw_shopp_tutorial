import { ShoppingCartService } from './../shopping-cart.service';
import { Component, Input } from '@angular/core';
import { Product } from '../models/product';


@Component({
  selector: 'shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent{
  shoppingCartItemCount: number;
  res :Product[]=[];
  Totalprice : number=0;
  @Input('produs') produs :Product;
  
   constructor(private shoppingCartService: ShoppingCartService,
    private cartService: ShoppingCartService)
    {
    let cart$ = this.shoppingCartService.getCart();
    cart$.snapshotChanges().subscribe(cart =>{
      this.shoppingCartItemCount = 0;
      if(cart.payload.exists()){
        let x=cart.payload.exportVal().items;
        for (var i in x) {
          this.res.push(x[i])
          this.shoppingCartItemCount += x[i].quantity;
          this.Totalprice+= x[i].quantity*x[i].product.price; 
          
        }
      }
    });
   }
  addToCart(product){
     this.res=[];
     this.Totalprice=0;
    this.cartService.addToCart(product);
  } 
  removeFromCart(product){
    this.res=[];
    this.Totalprice=0;
    this.cartService.removeFromCart(product);
  }
  clearCart(){
    this.res=[];
    this.Totalprice=0;
    this.shoppingCartService.clearCart();
  }
}
