import { UserService } from './../user.service';
import { AuthService } from './../auth.service';
import { OrderService } from './../order.service';
import { Component } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent {
  orders$;
  user$;
  orders =[];
  userId;
  constructor(
    private authService: AuthService,
    private orderService: OrderService,
    private afAuth: AngularFireAuth,
    private userService: UserService) { 
      authService.user$.subscribe(user => {
    this.userId = user.uid;
      this.orders$ = orderService.getOrdersByUser(this.userId).snapshotChanges().forEach(order =>{
        order.map(dt=>{
          this.orders.push({
            key: dt.payload.key,
            items : dt.payload.exportVal().items,
            quantity : dt.payload.exportVal().quantity,
            shipping : dt.payload.exportVal().shipping,
            totalPrice : dt.payload.exportVal().totalPrice,
            datePlaced : dt.payload.exportVal().datePlaced,
          });
        });
      });
    });
  }
}
